# api-proxy-server

Proxy app for ECS investigation task

## Usage

### Env Variables

* 'LISTEN_PORT' - Port to listen on (default:'8000')
* 'APP_HOST' - Hostname pf the app to forward request to (default: 'api')
* 'APP_PORT' - Port of rhe app to forward requests to (default: '9000')